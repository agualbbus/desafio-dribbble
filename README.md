# Desafio Dribble #

O app carrega shots mais populares do  Dribbble. 
[https://desafiodribbble.herokuapp.com](https://desafiodribbble.herokuapp.com "Desafio Dribbble")

## Funcionalidades e características : ##

* Ruby on Rails 4
* Cadastro e login
* Twitter Bootstrap
*  Carregamento assincronico  dos shots (paginação cada 30 shots).

## API bug: ##
A API do Dribbble tem um bug esquisito na hora de paginar, ela traz elementos repetidos de paginas anteriores....

## Clonar: ##
Para o App funcionar num ambiente local tenha em conta que ele foi desenvolvido no Windows x64 e a base de dados e config não estão incluídos no Repo, tera que criar a base e executar  `rake db:migrate`.