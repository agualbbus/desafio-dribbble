# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
ready =->
  
  document.L=$('#ligthbox')
  document.Sc=$('.shot-container')
  document.LightClose=$('.close-lightbox')
  
  document.LightClose.on 'click', (e)->
      document.L.toggleClass('active')    
  
  document.ligthBox=(selector)->
    selector.on 'click', (e)->
      $this=$(this)
      shot=$this.attr('data-shot')
      
      $.ajax({
        url:'/shots/'+shot
        success:(data)->
          document.L.toggleClass('active')
          document.Sc.empty().append(data)
      })
  
  document.ligthBox($('.shot'))

  $('.list-cont').jscroll({
      loadingHtml: '<i class="fa fa-circle-o-notch fa-spin"></i>',
      padding: 20,
      nextSelector: '.next-page'
  });    
  
$(document).ready(ready)
$(document).on('page:load', ready)