class ShotsController < ApplicationController
  layout "blank", only: [:show]  
  def show
    @shot=Dribbble::Shot.find(params[:id].to_i)
  end
end
