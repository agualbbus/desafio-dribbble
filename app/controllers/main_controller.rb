class MainController < ApplicationController
  def index  
    if signed_in?   
      @shots=Dribbble::Shot.popular(:page => 1, :per_page => 24)
      @page=1;
    else
      redirect_to signin_path
    end
  end  
end
