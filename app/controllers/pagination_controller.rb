class PaginationController < ApplicationController
  layout "blank", only: [:next] 
  def next
    @page=params[:page]
    @shots=Dribbble::Shot.popular(:page => @page, :per_page => 24)
  end
  
  private
    def paginateParams
      params.permit(:page)
    end  

end
