class UsersController < ApplicationController
  
  layout "blank", only: [:edit, :update] 
  
  def index
    if is_admin?
      @users=User.all()
    else
      redirect_to root_path
    end
  end
  
  def edit
    if is_admin?
      @user=User.find(params[:id])
      render template: "users/edit"
    end  
  end  
  
  def update
    if is_admin?
        @user=User.find(params[:id])
        @user.update(update_user_params)
        flash[:messages] = {success: "Usuario editado com succeso!"}
        redirect_to users_path
    elsif  @current_user.id == params[:id] && signed_in? 

    end
  end

  def  destroy
    if is_admin?
      @user=User.find(params[:id]).destroy
      if @user.destroyed?
        flash[:messages] = {success: "Usuario apagado"}
        redirect_to users_url
      end
    end
  end
  
  def new
    @user=User.new
  end
  
  def home
  end
  
  def show
    if signed_in? 
    else
      redirect_sign_in     
    end    
  end
  
  
  def create
    @user = User.new(user_params)
    if @user.save
      flash[:messages] = {success: "casdastro ok!"}
      #UserJob.new.async.welcome(@user) #jobs/user_job
      #UserJob.new.async.notify_admin_new_user(@user) 
      #@admin=User.find_by!(role: "admin")
      #UserMailer.notify_admin_new_user(@user, @admin)
      
      redirect_to root_path
    else
     render 'new'   
    end
  end
  
  private
    def user_params
      params.require(:user).permit(
                      :name, 
                      :email, 
                      :password, 
                      :password_confirmation
                      )

    end
    
end
